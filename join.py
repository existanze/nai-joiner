
import argparse
import os.path
import datetime
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter, column_index_from_string


runtime = datetime.datetime.now().strftime('%Y%H%M%S')

ap = argparse.ArgumentParser()


ap.add_argument("-m","--main",required=True, help="This is the main xlsx file containing the properties")
ap.add_argument("-p","--properties",required=True, help="This is the folder location of the property files")


args = vars(ap.parse_args())


main = args['main']
pf = args['properties']

if not os.path.isfile(main):
    print("The file %s does not exist " % main)


if not os.path.isdir(pf):
    print("The path %s is not a directory " % pf)


dest = os.path.dirname(main)+'-'+runtime+".xlsx"

wb = load_workbook(main)


sheet = wb[wb.sheetnames[0]]

mapping = []
for cellObj in sheet['I4':'AT4']:
    for cell in cellObj:
        if cell.value:
            mapping.append((cell.column_letter,cell.value))



properties = []
for cellObj in sheet['G5':'G'+str(sheet.max_row)]:
    for cell in cellObj:
        if cell.value:
            properties.append(
                (cell.row,cell.value)
            )

data=[]

for property in properties:


    property_path = pf+os.path.sep+property[1]+".xlsx"
    if os.path.isfile(property_path):
        print("Property %s found in %s " % (property,property_path))

        property_data=[]
        wp = load_workbook(property_path,data_only=True)

        wsheet = wp[wp.sheetnames[0]]
        for v in mapping:
            try:
                property_data.append((v[0]+''+str(property[0]), wsheet[v[1]].value))
            except ValueError:
                print("%s => %s Error in value",property_path,v)

        wp.close()
        data.append((property[1],property_data))


for d in data:

    id = d[0]
    cells = d[1]
    for cell in cells:
        sheet[cell[0]]=cell[1]


print("Saving into %s" % dest)
wb.save(dest)
